<?php

include_once 'model/ambient.php';

class ambientController{
    public $page_title;
	public $view;
    public $site_title;

    public function __construct(){
        $this->view = 'home_view';
        $this->page_title = 'GestAmb';
        $this->site_title = "Gestor de ambientes azure de soporte";
        $this->ambObj = new Ambient();
    }

    public function ini(){
        $this->view = 'home_view';
    }
    
    public function getAll() {
        $this->view = 'home_view';
        $select = $this->ambObj->getAllAmb();        
        return $select;
    }
    
    public function getAmbientById() {
        $this->view = 'detail_view';
        $data = $this->ambObj->getById($_POST);
        return $data;
    }
    
    public function insertAmbient() {
        $this->view = 'create_view';
        $this->page_title = 'GestAmb';
        $this->site_title = "Gestor de ambientes azure de soporte";        
        if ($_POST) {
            $create = $this->ambObj->preInsertAmb($_POST);
            if ($create)
            if (@@$create['cont'] > 0) {
                $_GET['response'] = 1; //ya existe
                return $_GET['response'];
            }else{  
                $create = $this->ambObj->InsertAmb($_POST);
                if (!$create) {
                    $_GET['response'] = 2; //no se inserto
                    return $_GET['response'];
                }else {
                    $_GET['response'] = 3; //se inserto
                    return $_GET['response'];
                }
           }
        }
    }
    
}
?>