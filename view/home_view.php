<nav class="navbar navbar-light bg-light">
  <form class="container-fluid justify-content-start">
    <button class="btn btn-outline-dark me-2" type="button">(っ▀¯▀)つ MigraAropesMachine</button>
    
    <a type="button" class="btn btn-sm btn-outline-secondary"  href="index.php?controller=ambient&action=insertambient">(∩｀-´)⊃━☆ﾟ.*･｡ﾟ  Nueva Migra​​</a>
  </form>
</nav>

<div class="container">
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col" width="1"></th>
      <th scope="col" width="90"></th>
      <th scope="col">Cliente</th>
      <th scope="col">Pais</th>
      <th scope="col">Entorno</th>      
      <th scope="col">CodAmb</th>
      <th scope="col">Version</th>
      <th scope="col">Branch.HF</th>
      <th scope="col">Ticket</th>
      <th scope="col" width="50" >FE</th>
      <th scope="col" width="50">LA</th>
      <th scope="col" width="50">MS</th>
      <th scope="col" width="50">UT</th>
    </tr>
  </thead>
  <tbody>
  	<?php 
  	    if(is_countable($dataToView["data"]) && count($dataToView["data"]) > 0)
  	    foreach ($dataToView["data"] as $amb):?>  	    
  	    <tr>
          <th scope="row">
          <button class="btn btn-primary viewAmbient btn-sm" data-bs-toggle="modal" data-bs-target="#myModal" value="<?php print $amb["idambiente"]; ?>"><i class="bi bi-eye-fill"></i></button></th>
          <td><button class="btn btn-secondary editAmbient btn-sm" data-bs-toggle="modal" data-bs-target="#myModal" value="<?php print $amb["idambiente"]; ?>"><i class="bi bi-pencil-square"></i></button></td>
          <td><?php print $amb['cliente']?></td>
          <td><?php echo $amb['pais']?></td>
          <td><?php echo $amb['entorno']?></td>          
          <td><?php echo $amb['codambiente']?></td>
          <td><?php echo $amb['VERSION']?></td>
          <td><?php echo $amb['branch']?></td>
          <td><?php echo $amb['ticket']?></td>
    	  <td><a type="button" class="btn btn-danger btn-sm" href="<?php echo $amb['inbroker5']; ?>" target="_blank"><i class="bi bi-link-45deg"></i></a></td>
    	  <td><a type="button" class="btn btn-warning btn-sm" href="<?php echo $amb['logaudit']; ?>" target="_blank"><i class="bi bi-link-45deg"></i></a></td>
    	  <td><a  class="btn btn-success btn-sm" href="<?php echo $amb['modseg']; ?>" target="_blank"><i class="bi bi-link-45deg"></i></a></td>
    	  <td><a type="button" class="btn btn-info btn-sm" href="<?php  echo $amb['ut']; ?>" target="_blank"><i class="bi bi-link-45deg"></i></a></td>
        </tr>  	    
  	<?php endforeach; ?>   
  </tbody>
</table>
</div>
<script>
$(document).ready(function() {


	$('.viewAmbient').on('click',function(){
	    let idAmbiente = $(this).attr('value');	
	    $('.modal-content').load(
	        'index.php?controller=ambient&action=getAmbientById',
	        { "id": idAmbiente },
	        function(){
	            $('#myModal').modal({show:true});
	        }
	    );
	});


$('.editAmbient').on('click',function(){
	    let idAmbiente = $(this).attr('value');
	    $('.modal-content').load(
	        'index.php?controller=ambient&action=',
	        { "id": idAmbiente },
	        function(){
	            $('#myModal').modal({show:true});
	        }
	    );
	});
	

});
</script>