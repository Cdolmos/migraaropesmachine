<?php if(is_countable($dataToView["data"]) && count($dataToView["data"]) > 0)?>
<div class="modal-header">    
    <h5 class="modal-title"><?php print $dataToView["data"]['codambiente']." - ".$dataToView["data"]['cliente'] ?></h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="modal-body">
	<form class="row g-3">
      <div class="col-md-4">
        <label for="inputEmail4" class="form-label" >Cliente</label>
        <input disabled type="text" class="form-control" id="inputEmail4" name="cliente" value="<?php print $dataToView["data"]['cliente'];?>">
      </div>
      <div class="col-md-4">
        <label for="inputPassword4" class="form-label" >Pais</label>
        <input disabled type="text" class="form-control" id="inputPassword4" name="pais" value="<?php print $dataToView["data"]['pais'];?>">
      </div>
      <div class="col-md-4">
        <label for="inputPassword4" class="form-label" >Entorno</label>
        <select disabled id="inputState" class="form-select" name="entorno" value="<?php print $dataToView["data"]['entorno'];?>">
        		<option value="PRD">PRD</option>
        		<option value="QA">QA</option>
        		<option value="UAT">UAT</option>
        	</select>
      </div>
      <div class="col-4">
        <label for="inputAddress" class="form-label">Ticket de pedido</label>
        <input disabled type="text" class="form-control" id="inputAddress" name="ticket" value="<?php print $dataToView["data"]['ticket'];?>">
      </div>
      <div class="col-4">
        <label for="inputAddress2" class="form-label" >CodAmbiente</label>
        <input disabled type="text" class="form-control" id="inputAddress2" name="codambiente" value="<?php print $dataToView["data"]['codambiente'];?>">
      </div>
      <div class="col-md-4">
        <label for="inputCity" class="form-label" >Version</label>
        <input disabled type="text" class="form-control" id="inputCity" name="version" value="<?php print $dataToView["data"]['version'];?>">
      </div>
      <div class="col-md-4">
        <label for="inputZip" class="form-label">Branch</label>
        <input disabled type="text" class="form-control" id="inputZip" name="branch" value="<?php print $dataToView["data"]['branch'];?>">
      </div>
      <div class="col-md-4">
        <label for="inputZip" class="form-label">HF</label>
        <input disabled type="text" class="form-control" id="inputZip" name="hf" value="<?php print $dataToView["data"]['hotfix'];?>">
      </div>
      <div class="col-4">
        <label for="inputZip" class="form-label"  >Owner</label>
        <input disabled type="text" class="form-control" id="inputZip" name="owner" value="<?php print $dataToView["data"]['owner'];?>">
      </div>

    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary">Save changes</button>
</div>
