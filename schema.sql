-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         8.0.30 - MySQL Community Server - GPL
-- SO del servidor:              Win64
-- HeidiSQL Versión:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Volcando estructura para tabla migracionarope.ambiente
CREATE TABLE IF NOT EXISTS `ambiente` (
  `idambiente` int NOT NULL AUTO_INCREMENT,
  `cliente` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `pais` varchar(30) COLLATE utf8mb4_general_ci NOT NULL,
  `entorno` varchar(20) COLLATE utf8mb4_general_ci NOT NULL,
  `ticket` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `codambiente` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `version` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `branch` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `inbroker5` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `logaudit` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `modseg` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ut` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `backend` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `hp` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `hr` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `hs` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `usuario` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `contrasena` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `servidorbd` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `basededatos` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `usuariobd` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `contrasenabd` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `urladjunto` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`idambiente`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- La exportación de datos fue deseleccionada.

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
