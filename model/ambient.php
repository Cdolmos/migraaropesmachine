<?php

class ambient {
    
    private $table = 'ambiente';
    private $conection;
    public $cliente;
    public $pais;
    public $entorno;
    public $ticket;
    public $codAmbiente;
    public $version;
    public $branch;
    public $hf;
    public $owner;
    
    public function __construct() {
        
    }
    
    /* Set conection */
    public function getConection(){
        $dbObj = new Db();
        $this->conection = $dbObj->conection;
    }
    
    public function getAllAmb() {
        $this->getConection();
        $sql = "Select idambiente,cliente,pais,entorno,ticket,codambiente,VERSION,branch,inbroker5,logaudit,modseg,ut from ".$this->table." order by cliente asc";        
        $stmt = $this->conection->prepare($sql);
        $stmt->execute();
        $all = $stmt->fetchAll();        
        return $all;        
    }
    
    public function getById() {
        $id = $_POST['id'];
        $this->getConection();
        $sql = "SELECT * FROM ".$this->table." WHERE idambiente = ?";
        $stmt = $this->conection->prepare($sql);
        $stmt->execute([$id]);
        $exec = $stmt->fetch();
        return $exec;
    }
    
    public function preInsertAmb(){
        $codAmbiente = $_POST['codambiente'];        
        $this->getConection();
        $sql = "SELECT COUNT(1) as cont FROM ".$this->table." WHERE codambiente = ?";
        $stmt = $this->conection->prepare($sql);
        $stmt->execute([$codAmbiente]);        
        return $stmt->fetchAll();        
    }    
     
    public function insertAmb() {        
        $cliente = $_POST['cliente'];
        $pais = $_POST['pais'];
        $entorno = $_POST['entorno'];
        $ticket = $_POST['ticket'];
        $codAmbiente = $_POST['codambiente'];
        $version = $_POST['version'];
        $branch = $_POST['branch'];
        $inbk = $_POST['inbroker'];
        $la = $_POST['logaudit'];
        $ms = $_POST['modseg'];
        $ut = $_POST['ut'];
        $back = $_POST['backend'];
        $hp = $_POST['hp'];
        $hr = $_POST['hr'];
        $hs = $_POST['hs'];
        $usrapp = $_POST['usuario'];
        $passapp = $_POST['contrasena'];
        $server = $_POST['servidor'];
        $bd = $_POST['bd'];
        $usrbd = $_POST['usuariobd'];
        $passbd = $_POST['constrasenabd'];        
        
        $this->getConection();
        $sql = "INSERT INTO ".$this->table." (cliente,pais,entorno,ticket,codambiente,VERSION,branch,inbroker5,logaudit,modseg,ut,backend,hp,hr,hs,usuario,contrasena,servidorbd,basededatos,usuariobd,contrasenabd)";
        $sql .= " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = $this->conection->prepare($sql);
        $exec = $stmt->execute([$cliente,$pais,$entorno,$ticket,$codAmbiente,$version,$branch,$inbk,$la,$ms,$ut,$back,$hp,$hr,$hs,$usrapp,$passapp,$server,$bd,$usrbd,$passbd]);         
        return $exec;
     }
    

}

?>
